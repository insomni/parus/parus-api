#!/bin/bash
set -e
set -x

## Подключить дополнения
for extension in /tmp/postgres/extensions/*.psql
do
    psql -U ${DB_USER} -d ${DB_NAME} < ${extension}
done;

## Создать таблицы
for table in /tmp/postgres/tables/*.psql
do
    psql -U ${DB_USER} -d ${DB_NAME} < ${table}
done;

## Создать функции
for func in /tmp/postgres/functions/**/*.psql
do
    psql -U ${DB_USER} -d ${DB_NAME} < ${func}
done;

## Создать ограничения
for constraint in /tmp/postgres/constraints/**/*.psql
do
    psql -U ${DB_USER} -d ${DB_NAME} < ${constraint}
done;

## Создать триггеры
for trigger in /tmp/postgres/triggers/*.psql
do
    psql -U ${DB_USER} -d ${DB_NAME} < ${trigger}
done;

## Импортировать данные
for csv in /tmp/postgres/dicts/*.csv
do
    table_name=${csv%.*}
    table_name=${table_name##*/}
    psql -U ${DB_USER} -d ${DB_NAME} -c "COPY ${table_name} FROM '${csv}' CSV HEADER;"
done
