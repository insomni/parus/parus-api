from datetime import datetime
from typing import Type

from flask import Blueprint, request, Response

from lib.helpers.api import BaseBlueprint
from lib.helpers.api.blueprint import return_many
from parus_api.serializers import NotificationSerializer
from parus_api.services import NotificationsService


class NotificationsBlueprint(BaseBlueprint[NotificationsService]):

    def __init__(self, service: NotificationsService):
        super().__init__(service)

    @property
    def _serializer(self) -> Type[NotificationSerializer]:
        return NotificationSerializer

    @property
    def _name(self) -> str:
        return 'notifications'

    def _create_blueprint(self) -> Blueprint:
        bp = Blueprint(self._name, __name__)

        @bp.route('/', methods=['POST'])
        def add():
            return self._add()

        @bp.route('/', methods=['PUT'])
        def update():
            return self._update()

        @bp.route('/', methods=['GET'])
        def get_all():
            return self._get_all()

        @bp.route('/<uid>', methods=['GET'])
        def get_by_id(uid):
            return self._get_by_id(uid)

        @bp.route('/user/<uid>', methods=['GET'])
        def get_by_user(uid):
            models = self._service.get_by_user(uid)
            return return_many(self._serializer, models, 200)

        @bp.route('/user/<user_uid>/deactivate', methods=['PUT'])
        def deactivate_by_date(user_uid):
            data = request.json
            dt_start = None if data.get('dt_start') in (None, 'null') else data['dt_start']
            dt_end = datetime.now() if data.get('dt_end') in (None, 'null') else datetime.fromisoformat(data['dt_end'])
            self._service.set_active_by_date(user_uid, dt_start, dt_end)
            return Response(response='', status=200, mimetype='application/json')

        @bp.route('/<uid>', methods=['DELETE'])
        def delete(uid):
            return self._delete(uid)

        return bp
