import json

from flask import Blueprint, request, abort

from lib.helpers.api import BaseBlueprint
from lib.helpers.api.blueprint import return_one
from parus_api.serializers import UserSerializer
from parus_api.services import UsersService


class AuthBlueprint(BaseBlueprint[UsersService]):

    def __init__(self, service: UsersService):
        super().__init__(service)

    @property
    def _serializer(self):
        raise UserSerializer

    @property
    def _name(self) -> str:
        return 'login'

    def _create_blueprint(self) -> Blueprint:
        bp = Blueprint(self._name, __name__)

        @bp.route('/login', methods=['POST'])
        def login():
            data = request.json
            username = data['username']
            password = data['password']
            user = self._service.get_by_credentials(username, password)
            if user:
                return return_one(UserSerializer, user, 200)
            else:
                abort(404)

        return bp
