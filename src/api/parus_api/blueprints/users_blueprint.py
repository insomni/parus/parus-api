from typing import Type

from flask import Blueprint

from lib.helpers.api import BaseBlueprint
from parus_api.models import UserModel
from parus_api.serializers import UserSerializer
from parus_api.services import UsersService


class UsersBlueprint(BaseBlueprint[UsersService]):

    def __init__(self, service: UsersService):
        super().__init__(service)

    @property
    def _serializer(self) -> Type[UserSerializer]:
        return UserSerializer

    @property
    def _name(self) -> str:
        return 'users'

    def _create_blueprint(self) -> Blueprint:
        bp = Blueprint(self._name, __name__)

        @bp.route('/', methods=['POST'])
        def add():
            return self._add()

        @bp.route('/', methods=['PUT'])
        def update():
            return self._update()

        @bp.route('/', methods=['GET'])
        def get_all():
            return self._get_all()

        @bp.route('/<uid>', methods=['GET'])
        def get_by_id(uid):
            return self._get_by_id(uid)

        @bp.route('/<uid>', methods=['DELETE'])
        def delete(uid):
            return self._delete(uid)

        return bp
