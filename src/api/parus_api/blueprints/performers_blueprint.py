from typing import Type

from flask import Blueprint

from lib.helpers.api import BaseBlueprint
from lib.helpers.api.blueprint import return_many
from parus_api.serializers import PerformerSerializer
from parus_api.services import PerformersService


class PerformersBlueprint(BaseBlueprint[PerformersService]):

    def __init__(self, service: PerformersService):
        super().__init__(service)

    @property
    def _serializer(self) -> Type[PerformerSerializer]:
        return PerformerSerializer

    @property
    def _name(self) -> str:
        return 'performers'

    def _create_blueprint(self) -> Blueprint:
        bp = Blueprint(self._name, __name__)

        @bp.route('/', methods=['POST'])
        def add():
            return self._add()

        @bp.route('/', methods=['PUT'])
        def update():
            return self._update()

        @bp.route('/', methods=['GET'])
        def get_all():
            return self._get_all()

        @bp.route('/<uid>', methods=['GET'])
        def get_by_id(uid):
            return self._get_by_id(uid)

        @bp.route('/performer/<uid>', methods=['GET'])
        def get_by_performer(uid):
            models = self._service.get_by_performer(uid)
            return return_many(self._serializer, models, 200)

        @bp.route('/task/<uid>', methods=['GET'])
        def get_by_task(uid):
            models = self._service.get_by_task(uid)
            return return_many(self._serializer, models, 200)

        @bp.route('/<uid>', methods=['DELETE'])
        def delete(uid):
            return self._delete(uid)

        return bp
