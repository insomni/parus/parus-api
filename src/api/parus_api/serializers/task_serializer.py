import json
from datetime import datetime
from typing import Dict
from uuid import uuid4, UUID

from flask import url_for

from lib.helpers.api import BaseSerializer
from parus_api.models import TaskModel
from parus_api.serializers import UserSerializer


class TaskSerializer(BaseSerializer):

    @staticmethod
    def dump(model: TaskModel) -> Dict:
        data = dict()

        data.update({
            'uid': model.uid.hex,
            'href': url_for('tasks.get_by_id', uid=model.uid.hex)
        })

        if model.is_filled:
            data.update({
                'data': json.loads(model.data),
                'dt_start': model.dt_start.isoformat(),
                'dt_end': model.dt_end.isoformat() if model.dt_end else None,
                'status': model.status,
                'creator': UserSerializer.dump(model.creator)
            })

        return data

    @staticmethod
    def load(data: Dict) -> TaskModel:
        uid = uuid4() if data.get('uid') in (None, 'null') else UUID(data['uid'])
        model = TaskModel(uid)
        model.fill(
            data=json.dumps(data['data']),
            dt_start=datetime.now() if data.get('dt_start') in (None, 'null') else datetime.fromisoformat(data['dt_start']),
            dt_end=None if data.get('dt_end') in (None, 'null') else datetime.fromisoformat(data['dt_end']),
            status=data['status'],
            creator_id=data['creator_id']
        )
        return model
