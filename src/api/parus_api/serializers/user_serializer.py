from typing import Dict
from uuid import uuid4, UUID

from flask import url_for

from lib.helpers.api import BaseSerializer
from parus_api.models import UserModel, UserPostModel


class UserSerializer(BaseSerializer):

    @staticmethod
    def dump(model: UserModel) -> Dict:
        data = dict()

        data.update({
            'uid': model.uid.hex,
            'href': url_for('users.get_by_id', uid=model.uid.hex)
        })

        if model.is_filled:
            data.update({
                'username': model.username,
                'hash_pw': model.hash_pw,
                'token': model.token,
                'is_guest': model.is_guest,
                'user_group': model.user_group
            })

        return data

    @staticmethod
    def load(data: Dict) -> UserPostModel:
        uid = uuid4() if data.get('uid') in (None, 'null') else UUID(data['uid'])
        model = UserPostModel(uid)
        model.fill(
            username=data['username'],
            password=data['password'],
            token=data['token'] if data.get('token') not in (None, 'null') else None,
            is_guest=data['is_guest'] if data.get('is_guest') not in (None, 'null') else False,
            user_group=data['user_group'] if data.get('user_group') not in (None, 'null') else 1
        )
        return model
