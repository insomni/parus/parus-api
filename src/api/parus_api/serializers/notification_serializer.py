from datetime import datetime
from typing import Dict
from uuid import uuid4, UUID

from flask import url_for

from lib.helpers.api import BaseSerializer
from parus_api.models import NotificationModel
from parus_api.serializers import UserSerializer, TaskSerializer


class NotificationSerializer(BaseSerializer):

    @staticmethod
    def dump(model: NotificationModel) -> Dict:
        data = dict()

        data.update({
            'uid': model.uid.hex,
            'href': url_for('notifications.get_by_id', uid=model.uid.hex)
        })

        if model.is_filled:
            data.update({
                'user': UserSerializer.dump(model.user),
                'task': TaskSerializer.dump(model.task),
                'text': model.text,
                'type': model.type,
                'dt': model.dt.isoformat(),
                'is_active': model.is_active
            })

        return data

    @staticmethod
    def load(data: Dict) -> NotificationModel:
        uid = uuid4() if data.get('uid') in (None, 'null') else UUID(data['uid'])
        model = NotificationModel(uid)
        model.fill(
            user_id=data['user_id'],
            task_id=data['task_id'],
            text=None if data.get('text') in (None, 'null') else data['text'],
            type=data['type'],
            dt=datetime.now() if data.get('dt') in (None, 'null') else datetime.fromisoformat(data['dt']),
            is_active=data['is_active']
        )
        return model
