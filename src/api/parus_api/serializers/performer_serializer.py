from typing import Dict
from uuid import uuid4, UUID

from flask import url_for

from lib.helpers.api import BaseSerializer
from parus_api.models import PerformerModel
from parus_api.serializers import UserSerializer, TaskSerializer


class PerformerSerializer(BaseSerializer):

    @staticmethod
    def dump(model: PerformerModel) -> Dict:
        data = dict()

        data.update({
            'uid': model.uid.hex,
            'href': url_for('performers.get_by_id', uid=model.uid.hex)
        })

        if model.is_filled:
            data.update({
                'performer': UserSerializer.dump(model.performer),
                'task': TaskSerializer.dump(model.task)
            })

        return data

    @staticmethod
    def load(data: Dict) -> PerformerModel:
        uid = uuid4() if data.get('uid') in (None, 'null') else UUID(data['uid'])
        model = PerformerModel(uid)
        model.fill(
            performer_id=data['performer_id'],
            task_id=data['task_id']
        )
        return model
