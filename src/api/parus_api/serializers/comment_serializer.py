from datetime import datetime
from typing import Dict
from uuid import uuid4, UUID

from flask import url_for

from lib.helpers.api import BaseSerializer
from parus_api.models import CommentModel
from parus_api.serializers import UserSerializer, TaskSerializer


class CommentSerializer(BaseSerializer):

    @staticmethod
    def dump(model: CommentModel) -> Dict:
        data = dict()

        data.update({
            'uid': model.uid.hex,
            'href': url_for('comments.get_by_id', uid=model.uid.hex)
        })

        if model.is_filled:
            data.update({
                'text': model.text,
                'dt': model.dt.isoformat(),
                'type': model.type,
                'creator': UserSerializer.dump(model.creator),
                'task': TaskSerializer.dump(model.task)
            })

        return data

    @staticmethod
    def load(data: Dict) -> CommentModel:
        uid = uuid4() if data.get('uid') in (None, 'null') else UUID(data['uid'])
        model = CommentModel(uid)
        model.fill(
            text=data['text'],
            dt=datetime.now() if data.get('dt') in (None, 'null') else datetime.fromisoformat(data['dt']),
            type=data['type'],
            creator_id=data['creator_id'],
            task_id=data['task_id']
        )
        return model
