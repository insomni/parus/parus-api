from datetime import datetime
from uuid import UUID

from lib.helpers.api import Entity
from parus_api.models import UserModel, TaskModel


class CommentModel(Entity[UUID]):

    def __init__(self, uid: UUID):
        super().__init__(uid)
        self.text: str = None
        self.dt: datetime = None
        self.type: int = None
        self.creator: UserModel = None
        self.task: TaskModel = None

    def fill(self, text: str, dt: datetime, type: int, creator_id: UUID, task_id: UUID):
        self.text = text
        self.dt = dt
        self.type = type
        self.creator = UserModel(creator_id)
        self.task = TaskModel(task_id)
        self.filled()
        return self

    @classmethod
    def key_field(cls) -> str:
        return 'comment_id'
