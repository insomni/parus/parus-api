from uuid import UUID

from lib.helpers.api import Entity


class UserModel(Entity[UUID]):

    def __init__(self, uid: UUID):
        super().__init__(uid)
        self.username: str = None
        self.hash_pw: str = None
        self.token: str = None
        self.is_guest: bool = None
        self.user_group: int = None

    def fill(self, username: str, hash_pw: str, token: str, is_guest: bool,
             user_group: int):
        self.username = username
        self.hash_pw = hash_pw
        self.token = token
        self.is_guest = is_guest
        self.user_group = user_group
        self.filled()
        return self

    @classmethod
    def key_field(cls) -> str:
        return 'user_id'
