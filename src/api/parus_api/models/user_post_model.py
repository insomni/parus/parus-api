from click import UUID

from lib.helpers.api import Entity


class UserPostModel(Entity[UUID]):

    def __init__(self, uid: UUID):
        super().__init__(uid)
        self.username: str = None
        self.password: str = None
        self.token: str = None
        self.is_guest: bool = None
        self.user_group: int = None

    def fill(self, **kwargs):
        self.username = kwargs['username']
        self.password = kwargs['password']
        self.token = kwargs['token']
        self.is_guest = kwargs['is_guest']
        self.user_group = kwargs['user_group']
        self.filled()

    @classmethod
    def key_field(cls):
        raise NotImplementedError
