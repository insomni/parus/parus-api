from datetime import datetime
from uuid import UUID

from lib.helpers.api import Entity
from parus_api.models import UserModel, TaskModel


class NotificationModel(Entity[UUID]):

    def __init__(self, uid: UUID):
        super().__init__(uid)
        self.user: UserModel = None
        self.task: TaskModel = None
        self.text: str = None
        self.type: int = None
        self.dt: datetime = None
        self.is_active: bool = None

    def fill(self, user_id: UUID, task_id: UUID, text: str, type: int, dt: datetime,
             is_active: bool):
        self.user = UserModel(user_id)
        self.task = TaskModel(task_id)
        self.text = text
        self.type = type
        self.dt = dt
        self.is_active = is_active
        self.filled()
        return self

    @classmethod
    def key_field(cls) -> str:
        return 'notification_id'
