from datetime import datetime
from uuid import UUID

from lib.helpers.api import Entity
from parus_api.models import UserModel


class TaskModel(Entity[UUID]):

    def __init__(self, uid: UUID):
        super().__init__(uid)
        self.data: str = None
        self.dt_start: datetime = None
        self.dt_end: datetime = None
        self.status: int = None
        self.creator: UserModel = None

    def fill(self, data: str, dt_start: datetime, dt_end: datetime, status: int,
             creator_id: UUID):
        self.data = data
        self.dt_start = dt_start
        self.dt_end = dt_end
        self.status = status
        self.creator = UserModel(creator_id)
        self.filled()
        return self

    @classmethod
    def key_field(cls) -> str:
        return 'task_id'
