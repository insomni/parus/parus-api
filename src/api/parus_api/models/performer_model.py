from uuid import UUID

from lib.helpers.api import Entity
from parus_api.models import UserModel, TaskModel


class PerformerModel(Entity[UUID]):

    def __init__(self, uid: UUID):
        super().__init__(uid)
        self.performer: UserModel = None
        self.task: TaskModel = None

    def fill(self, performer_id: UUID, task_id: UUID):
        self.performer = UserModel(performer_id)
        self.task = TaskModel(task_id)
        self.filled()
        return self

    @classmethod
    def key_field(cls) -> str:
        return 'record_id'
