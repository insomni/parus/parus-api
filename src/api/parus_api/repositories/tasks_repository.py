from typing import List
from uuid import UUID

from lib.helpers.api import Repository, create_many, create_one
from lib.helpers.database import DatabasePostgres
from parus_api.models import TaskModel


class TasksRepository(Repository[DatabasePostgres]):

    def __init__(self, context):
        super().__init__(context)

    def add(self, entity: TaskModel) -> UUID:
        """Добавить задачу"""
        self._context.callproc('tasks_add', [entity.uid, entity.data, entity.dt_start,
                                             entity.dt_end, entity.status, entity.creator.uid])
        return entity.uid

    def update(self, entity: TaskModel) -> None:
        """Обновить данные задачи"""
        self._context.callproc('tasks_update', [entity.uid, entity.data, entity.dt_start,
                                                entity.dt_end, entity.status, entity.creator.uid])

    def get_all(self) -> List[TaskModel]:
        """Вернуть все задачи"""
        data = self._context.callproc('tasks_get_all', [])
        return create_many(TaskModel, data)

    def get_by_id(self, uid) -> TaskModel:
        """Вернуть задачу по uid"""
        data = self._context.callproc('tasks_get_by_id', [uid])
        return create_one(TaskModel, data)

    def get_by_creator(self, creator_uid) -> List[TaskModel]:
        """Вернуть задачи по uid их создателя"""
        data = self._context.callproc('tasks_get_by_creator', [creator_uid])
        return create_many(TaskModel, data)

    def delete(self, uid) -> None:
        """Удалить задачу по uid"""
        self._context.callproc('tasks_delete', [uid])
