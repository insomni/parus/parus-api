from typing import List
from uuid import UUID

from lib.helpers.api import Repository, create_many, create_one
from lib.helpers.database import DatabasePostgres
from parus_api.models import PerformerModel


class PerformersRepository(Repository[DatabasePostgres]):

    def __init__(self, context):
        super().__init__(context)

    def add(self, entity: PerformerModel) -> UUID:
        """Добавить исполнителя"""
        self._context.callproc('performers_add', [entity.uid, entity.performer.uid, entity.task.uid])
        return entity.uid

    def update(self, entity: PerformerModel) -> None:
        """Обновить данные исполнителя"""
        self._context.callproc('performers_update', [entity.uid, entity.performer.uid, entity.task.uid])

    def get_all(self) -> List[PerformerModel]:
        """Вернуть всех исполнителей"""
        data = self._context.callproc('performers_get_all', [])
        return create_many(PerformerModel, data)

    def get_by_id(self, uid) -> PerformerModel:
        """Вернуть исполнителя по uid"""
        data = self._context.callproc('performers_get_by_id', [uid])
        return create_one(PerformerModel, data)

    def get_by_performer(self, performer_uid) -> List[PerformerModel]:
        """Вернуть задачи по uid их исполнителя"""
        data = self._context.callproc('performers_get_by_performer', [performer_uid])
        return create_many(PerformerModel, data)

    def get_by_task(self, task_uid) -> List[PerformerModel]:
        """Вернуть исполнителей по uid задачи"""
        data = self._context.callproc('performers_get_by_task', [task_uid])
        return create_many(PerformerModel, data)

    def delete(self, uid) -> None:
        """Удалить исполнителя по uid"""
        self._context.callproc('performers_delete', [uid])
