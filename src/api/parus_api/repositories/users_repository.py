from typing import List
from uuid import UUID

from lib.helpers.api import Repository, create_many, create_one
from lib.helpers.database import DatabasePostgres
from parus_api.models import UserModel


class UsersRepository(Repository[DatabasePostgres]):

    def __init__(self, context):
        super().__init__(context)

    def add(self, entity: UserModel) -> UUID:
        """Добавить пользователя"""
        self._context.callproc('users_add', [entity.uid, entity.username, entity.hash_pw, entity.token,
                                             entity.is_guest, entity.user_group])
        return entity.uid

    def update(self, entity: UserModel) -> None:
        """Обновить данные пользователя"""
        self._context.callproc('users_update', [entity.uid, entity.username, entity.hash_pw, entity.token,
                                                entity.is_guest, entity.user_group])

    def get_all(self) -> List[UserModel]:
        """Вернуть всех пользователей"""
        data = self._context.callproc('users_get_all', [])
        return create_many(UserModel, data)

    def get_by_id(self, uid) -> UserModel:
        """Вернуть пользователя по uid"""
        data = self._context.callproc('users_get_by_id', [uid])
        return create_one(UserModel, data)

    def get_by_name(self, username):
        """Вернуть пользователя по имени"""
        data = self._context.callproc('users_get_token', [username])
        return create_one(UserModel, data)

    def delete(self, uid) -> None:
        """Удалить пользователя по uid"""
        self._context.callproc('users_delete', [uid])
