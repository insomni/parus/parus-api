from typing import List
from uuid import UUID

from lib.helpers.api import Repository, create_many, create_one
from lib.helpers.database import DatabasePostgres
from parus_api.models import NotificationModel


class NotificationsRepository(Repository[DatabasePostgres]):

    def __init__(self, context):
        super().__init__(context)

    def add(self, entity: NotificationModel) -> UUID:
        """Добавить уведомление"""
        self._context.callproc('notifications_add', [entity.uid, entity.user.uid, entity.task.uid, entity.text,
                                                     entity.type, entity.dt, entity.is_active])
        return entity.uid

    def update(self, entity: NotificationModel) -> None:
        """Обновить данные уведомления"""
        self._context.callproc('notifications_update', [entity.uid, entity.user.uid, entity.task.uid, entity.text,
                                                        entity.type, entity.dt, entity.is_active])

    def get_all(self) -> List[NotificationModel]:
        """Вернуть все уведомления"""
        data = self._context.callproc('notifications_get_all', [])
        return create_many(NotificationModel, data)

    def get_by_id(self, uid) -> NotificationModel:
        """Вернуть уведомление по uid"""
        data = self._context.callproc('notifications_get_by_id', [uid])
        return create_one(NotificationModel, data)

    def get_by_user(self, user_uid) -> List[NotificationModel]:
        """Вернуть уведомления по uid пользователя"""
        data = self._context.callproc('notifications_get_by_user', [user_uid])
        return create_many(NotificationModel, data)

    def set_active_by_date(self, user_id, dt_start, dt_end) -> None:
        """Установить уведомления между датами неактивными по uid пользователя"""
        self._context.callproc('notifications_set_active_by_dt', [user_id, dt_start, dt_end])

    def delete(self, uid) -> None:
        """Удалить уведомление по uid"""
        self._context.callproc('notifications_delete', [uid])
