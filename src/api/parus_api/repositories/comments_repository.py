from typing import List
from uuid import UUID

from lib.helpers.api import Repository, create_many, create_one
from lib.helpers.database import DatabasePostgres
from parus_api.models import CommentModel


class CommentsRepository(Repository[DatabasePostgres]):

    def __init__(self, context):
        super().__init__(context)

    def add(self, entity: CommentModel) -> UUID:
        """Добавить комментарий"""
        self._context.callproc('comments_add', [entity.uid, entity.text, entity.dt, entity.type, entity.creator.uid,
                                                entity.task.uid])
        return entity.uid

    def update(self, entity: CommentModel) -> None:
        """Обновить данные комментария"""
        self._context.callproc('comments_update', [entity.uid, entity.text, entity.dt, entity.type, entity.creator.uid,
                                                   entity.task.uid])

    def get_all(self) -> List[CommentModel]:
        """Вернуть все комментарии"""
        data = self._context.callproc('comments_get_all', [])
        return create_many(CommentModel, data)

    def get_by_id(self, uid) -> CommentModel:
        """Вернуть комментарий по uid"""
        data = self._context.callproc('comments_get_by_id', [uid])
        return create_one(CommentModel, data)

    def get_by_creator(self, creator_uid) -> List[CommentModel]:
        """Вернуть комментарии по uid их создателя"""
        data = self._context.callproc('comments_get_by_creator', [creator_uid])
        return create_many(CommentModel, data)

    def get_by_task(self, task_uid) -> List[CommentModel]:
        """Вернуть комментарии по uid задачи"""
        data = self._context.callproc('comments_get_by_task', [task_uid])
        return create_many(CommentModel, data)

    def delete(self, uid) -> None:
        """Удалить комментарий по uid"""
        self._context.callproc('comments_delete', [uid])
