from flask import request, abort

from src.api.parus_api.services import AuthService


def token_check(blueprint):
    auth_service = AuthService.get_instance()
    def decorator(*args, **kwargs):
        token = request.headers.get('Authorization')
        if token and auth_service.is_valid(token):
            blueprint(*args, **kwargs)
        else:
            abort(401)
    return decorator