from typing import List
from uuid import UUID

from lib.helpers.api import Service
from parus_api.models import PerformerModel
from parus_api.repositories import PerformersRepository


class PerformersService(Service[PerformerModel, PerformersRepository]):

    def __init__(self, repo: PerformersRepository):
        super().__init__(repo)

    def add(self, entity: PerformerModel) -> UUID:
        uid = self._add(entity)
        return uid

    def update(self, entity: PerformerModel) -> None:
        self._update(entity)

    def get_all(self) -> List[PerformerModel]:
        return self._get_all()

    def get_by_id(self, uid) -> PerformerModel:
        return self._get_by_id(uid)

    def get_by_performer(self, performer_uid) -> List[PerformerModel]:
        return self._repo.get_by_performer(performer_uid)

    def get_by_task(self, task_uid) -> List[PerformerModel]:
        return self._repo.get_by_task(task_uid)

    def delete(self, uid) -> None:
        return self._delete(uid)
