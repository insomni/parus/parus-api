from typing import List
from uuid import UUID

from lib.helpers.api import Service
from parus_api.models import CommentModel
from parus_api.repositories import CommentsRepository


class CommentsService(Service[CommentModel, CommentsRepository]):

    def __init__(self, repo: CommentsRepository):
        super().__init__(repo)

    def add(self, entity: CommentModel) -> UUID:
        uid = self._add(entity)
        return uid

    def update(self, entity: CommentModel) -> None:
        self._update(entity)

    def get_all(self) -> List[CommentModel]:
        return self._get_all()

    def get_by_id(self, uid) -> CommentModel:
        return self._get_by_id(uid)

    def get_by_creator(self, creator_uid) -> List[CommentModel]:
        return self._repo.get_by_creator(creator_uid)

    def get_by_task(self, task_uid) -> List[CommentModel]:
        return self._repo.get_by_task(task_uid)

    def delete(self, uid) -> None:
        return self._delete(uid)
