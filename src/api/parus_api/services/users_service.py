from typing import List, Optional
from uuid import uuid4, UUID

import bcrypt

from lib.helpers.api import Service
from parus_api.models import UserModel, UserPostModel
from parus_api.repositories import UsersRepository


class UsersService(Service[UserModel, UsersRepository]):

    def __init__(self, repo: UsersRepository):
        super().__init__(repo)

    def add(self, entity: UserPostModel) -> UUID:
        user = UserModel(entity.uid)
        hash_pw = bcrypt.hashpw(entity.password.encode(), bcrypt.gensalt()).decode()
        user.fill(
            username=entity.username,
            hash_pw=hash_pw,
            token=uuid4().hex,
            is_guest=entity.is_guest,
            user_group=entity.user_group
        )
        uid = self._add(user)
        return uid

    def update(self, entity: UserPostModel) -> None:
        if len(entity.password) > 0:
            hash_pw = bcrypt.hashpw(entity.password.encode(), bcrypt.gensalt()).decode()
        else:
            user_old = self._repo.get_by_id(entity.uid)
            hash_pw = user_old.hash

        user = UserModel(entity.uid)
        user.fill(
            username=entity.username,
            hash_pw=hash_pw,
            token=entity.token,
            is_guest=entity.is_guest,
            user_group=entity.user_group
        )
        self._update(user)

    def get_all(self) -> List[UserModel]:
        return self._get_all()

    def get_by_id(self, uid) -> UserModel:
        return self._get_by_id(uid)

    def get_by_credentials(self, username, password) -> Optional[UserModel]:
        user = self._repo.get_by_name(username)
        if bcrypt.checkpw(password.encode(), user.hash_pw.encode()):
            return user
        return None

    def delete(self, uid) -> None:
        return self._delete(uid)
