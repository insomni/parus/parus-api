from typing import List
from uuid import UUID

from lib.helpers.api import Service
from parus_api.models import TaskModel
from parus_api.repositories import TasksRepository


class TasksService(Service[TaskModel, TasksRepository]):

    def __init__(self, repo: TasksRepository):
        super().__init__(repo)

    def add(self, entity: TaskModel) -> UUID:
        uid = self._add(entity)
        return uid

    def update(self, entity: TaskModel) -> None:
        self._update(entity)

    def get_all(self) -> List[TaskModel]:
        return self._get_all()

    def get_by_id(self, uid) -> TaskModel:
        return self._get_by_id(uid)

    def get_by_creator(self, creator_uid) -> List[TaskModel]:
        return self._repo.get_by_creator(creator_uid)

    def delete(self, uid) -> None:
        return self._delete(uid)
