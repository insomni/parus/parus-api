from datetime import datetime
from typing import List
from uuid import UUID

from lib.helpers.api import Service
from parus_api.models import NotificationModel
from parus_api.repositories import NotificationsRepository


class NotificationsService(Service[NotificationModel, NotificationsRepository]):

    def __init__(self, repo: NotificationsRepository):
        super().__init__(repo)

    def add(self, entity: NotificationModel) -> UUID:
        uid = self._add(entity)
        return uid

    def update(self, entity: NotificationModel) -> None:
        self._update(entity)

    def get_all(self) -> List[NotificationModel]:
        return self._get_all()

    def get_by_id(self, uid) -> NotificationModel:
        return self._get_by_id(uid)

    def get_by_user(self, user_uid) -> List[NotificationModel]:
        return self._repo.get_by_user(user_uid)

    def set_active_by_date(self, user_uid, dt_start, dt_end) -> None:
        return self._repo.set_active_by_date(user_uid, dt_start, dt_end)

    def delete(self, uid) -> None:
        return self._delete(uid)
