import psycopg2
from typing import Tuple

from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
from psycopg2.extras import RealDictCursor
from psycopg2.psycopg1 import connection

from lib.helpers.database import DatabaseContext
from lib.helpers.database.Errors import DatabaseError, DatabaseConflictError, DatabaseProgrammingError


class DatabasePostgres(DatabaseContext):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        psycopg2.extras.register_uuid()

    def callproc(self, cmd, params):
        conn, cursor = self._connection
        try:
            cursor.callproc(cmd, params)
            data = cursor.fetchall()
        except psycopg2.IntegrityError as e:
            raise DatabaseConflictError(e.pgerror)
        except psycopg2.ProgrammingError as e:
            raise DatabaseProgrammingError(e.pgerror)
        except psycopg2.Error as e:
            raise DatabaseError(e.pgerror)
        finally:
            cursor.close()
            conn.close()
        return data

    def execute(self, cmd, params):
        conn, cursor = self._connection
        try:
            cursor.callproc(cmd, params)
            data = cursor.fetchall()
        except psycopg2.IntegrityError as e:
            raise DatabaseConflictError(e.pgerror)
        except psycopg2.ProgrammingError as e:
            raise DatabaseProgrammingError(e.pgerror)
        except psycopg2.Error as e:
            raise DatabaseError(e.pgerror)
        finally:
            cursor.close()
            conn.close()
        return data

    @property
    def schema(self):
        return 'postgresql'

    @property
    def _connection(self) -> Tuple[connection, RealDictCursor]:
        """Установить соединение"""
        conn = psycopg2.connect(self.conn_string)
        conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
        cursor = conn.cursor(cursor_factory=RealDictCursor)
        return conn, cursor
