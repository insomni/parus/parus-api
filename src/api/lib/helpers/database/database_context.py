class DatabaseContext(object):

    def __init__(self, host, port, dbname, uname, upass):
        self._host = host
        self._port = port
        self._dbname = dbname
        self._uname = uname
        self._upass = upass
        self._conn_string = None

    @property
    def conn_string(self):
        """Сформировать строку подключения"""
        if not self._conn_string:
            self._conn_string = f'{self.schema}://{self._uname}:{self._upass}@{self._host}:{self._port}/{self._dbname}'
        return self._conn_string

    @property
    def schema(self):
        raise NotImplementedError

    @property
    def _connection(self):
        raise NotImplementedError

    def callproc(self, cmd, params):
        raise NotImplementedError

    def execute(self, cmd, params):
        raise NotImplementedError
