from typing import Dict

from lib.helpers.api import Entity


class BaseSerializer(object):

    @staticmethod
    def dump(model: Entity) -> Dict:
        raise NotImplementedError

    @staticmethod
    def load(data: Dict) -> Entity:
        raise NotImplementedError
