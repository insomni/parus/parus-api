from typing import Dict, TypeVar, Generic

T = TypeVar('T')


class Entity(Generic[T]):

    def __init__(self, uid: T):
        self.uid: T = uid
        self._filled = False

    @property
    def is_filled(self) -> bool:
        return self._filled is True

    def filled(self):
        self._filled = True

    def fill(self, **kwargs) -> "Entity":
        raise NotImplementedError

    @classmethod
    def key_field(cls) -> str:
        raise NotImplementedError

    @classmethod
    def create(cls, data: Dict) -> "Entity":
        entity = cls(data.pop(cls.key_field()))
        entity.fill(**data)
        return entity


