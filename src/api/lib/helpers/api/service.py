from typing import Generic, TypeVar, Any, List

T = TypeVar('T')
R = TypeVar('R')


class Service(Generic[T, R]):

    def __init__(self, repo: R):
        self._repo = repo

    def _add(self, entity) -> Any:
        return self._repo.add(entity)

    def _update(self, entity) -> None:
        return self._repo.update(entity)

    def _get_all(self) -> List[T]:
        return self._repo.get_all()

    def _get_by_id(self, uid) -> T:
        return self._repo.get_by_id(uid)

    def _delete(self, uid) -> None:
        return self._repo.delete(uid)
