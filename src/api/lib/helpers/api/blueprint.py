import json

from typing import TypeVar, Generic, Any, List, Type

from flask import Blueprint, request, abort, Response, url_for

from lib.helpers.api import BaseSerializer
from lib.helpers.database.Errors import DatabaseError, DatabaseConflictError, DatabaseProgrammingError

S = TypeVar('S')


def return_one(serializer: Type[BaseSerializer], model: Any, status: int) -> Response:
    """
    Вернуть Response с сериализованной моделью
    :param serializer: Класс для сериализации объекта
    :param model: Модель для сериализации
    :param status: HTTP статус код возврата
    :return: Response, содержащий серализованный объект
    """
    data = serializer.dump(model)
    return Response(json.dumps(data), status, mimetype='application/json')


def return_many(serializer: Type[BaseSerializer], models: List[Any], status: int) -> Response:
    """
    Вернуть Response с массивом сериализованных моделей
    :param serializer: Класс для сериализации объектов
    :param models: Модели для сериализации
    :param status: HTTP статус код возврата
    :return: Response, содержащий json массив сериализованных объектов
    """
    data = []
    for model in models:
        data.append(serializer.dump(model))
    return Response(json.dumps(data), status, mimetype='application/json')


class BaseBlueprint(Generic[S]):

    def __init__(self, service: S):
        self._service = service
        self._blueprint = None

    @property
    def blueprint(self) -> Blueprint:
        if not self._blueprint:
            self._blueprint = self._create_blueprint()
        return self._blueprint

    @property
    def _serializer(self) -> Type[BaseSerializer]:
        raise NotImplementedError

    @property
    def _name(self) -> str:
        raise NotImplementedError

    def _create_blueprint(self) -> Blueprint:
        raise NotImplementedError

    def _parse(self) -> Any:
        entity = None
        try:
            entity = self._serializer.load(request.json)
        except BaseException as e:
            abort(500, e)
        return entity

    def _add(self) -> Response:
        entity = self._parse()
        uid = self._add_entity(entity)
        return Response(response='', status=201, headers={'Location': url_for('.get_by_id', uid=uid)},
                        mimetype='application/json')

    def _update(self) -> Response:
        entity = self._parse()
        model = self._update_entity(entity)
        if model is None:
            abort(404)
        return return_one(self._serializer, model, 200)

    def _get_all(self) -> Response:
        models = self._get_all_entities()
        return return_many(self._serializer, models, 200)

    def _get_by_id(self, uid: Any) -> Response:
        model = self._get_entity_by_id(uid)
        if model is None:
            abort(404)
        return return_one(self._serializer, model, 200)

    def _delete(self, uid: Any) -> Response:
        self._delete_entity(uid)
        return Response(response='', status=204, mimetype='application/json')

    def _add_entity(self, entity: Any) -> Any:
        uid = None
        try:
            uid = self._service.add(entity)
        except DatabaseConflictError as e:
            abort(409, e)
        except DatabaseProgrammingError as e:
            abort(418, e)
        except DatabaseError as e:
            abort(500, e)
        return uid

    def _update_entity(self, entity: Any) -> Any:
        try:
            self._service.update(entity)
        except DatabaseConflictError as e:
            abort(409, e)
        except DatabaseProgrammingError as e:
            abort(418, e)
        except DatabaseError as e:
            abort(500, e)
        model = self._get_entity_by_id(entity.uid)
        return model

    def _get_all_entities(self) -> List[Any]:
        entities = []
        try:
            entities = self._service.get_all()
        except DatabaseConflictError as e:
            abort(409, e)
        except DatabaseProgrammingError as e:
            abort(418, e)
        except DatabaseError as e:
            abort(500, e)
        return entities

    def _get_entity_by_id(self, uid) -> Any:
        entity = None
        try:
            entity = self._service.get_by_id(uid)
        except DatabaseConflictError as e:
            abort(409, e)
        except DatabaseProgrammingError as e:
            abort(418, e)
        except DatabaseError as e:
            abort(500, e)
        return entity

    def _delete_entity(self, uid) -> None:
        try:
            self._service.delete(uid)
        except DatabaseConflictError as e:
            abort(409, e)
        except DatabaseProgrammingError as e:
            abort(418, e)
        except DatabaseError as e:
            abort(500, e)
