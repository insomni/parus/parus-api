from typing import TypeVar, Generic, Any, List, Type, Optional

C = TypeVar('C')
T = TypeVar('T')


def create_one(cls: Type[T], data: List[Any]) -> Optional[T]:
    """
    Сформировать модель данных заданного класса
    :param cls: Класс модели
    :param data: Данные для заполнения
    :return: Заполненная модель
    """
    if len(data) == 0:
        return None
    return cls.create(data[0])


def create_many(cls: Type[T], data: List[Any]) -> List[T]:
    """
    Сформировать массив моделей данных заданного класса
    :param cls: Класс модели
    :param data: Данные для заполнения
    :return: Массив заполненных моделей
    """
    entities = []
    for row in data:
        entities.append(cls.create(row))
    return entities


class Repository(Generic[C]):

    def __init__(self, context: C):
        self._context = context

    def add(self, entity: Any) -> Any:
        raise NotImplementedError

    def update(self, entity: Any) -> None:
        raise NotImplementedError

    def get_all(self) -> List[Any]:
        raise NotImplementedError

    def get_by_id(self, uid: Any) -> Any:
        raise NotImplementedError

    def delete(self, uid: Any) -> None:
        raise NotImplementedError
