from flask import Flask
from flask_cors import CORS

import config
from lib.helpers.database import DatabasePostgres
from parus_api.blueprints import UsersBlueprint, TasksBlueprint, PerformersBlueprint, CommentsBlueprint, \
    NotificationsBlueprint, AuthBlueprint
from parus_api.repositories import UsersRepository, TasksRepository, PerformersRepository, CommentsRepository, \
    NotificationsRepository
from parus_api.services import UsersService, TasksService, PerformersService, CommentsService, NotificationsService

db = DatabasePostgres(host=config.BaseConfig.DB_SERVICE,
                      port=config.BaseConfig.DB_PORT,
                      dbname=config.BaseConfig.DB_NAME,
                      uname=config.BaseConfig.DB_USER,
                      upass=config.BaseConfig.DB_PASS)


app = Flask(config.APP_NAME)
CORS(app)

users_repo = UsersRepository(db)
users_service = UsersService(users_repo)
users_blueprint = UsersBlueprint(users_service)

tasks_repo = TasksRepository(db)
tasks_service = TasksService(tasks_repo)
tasks_blueprint = TasksBlueprint(tasks_service)

performers_repo = PerformersRepository(db)
performers_service = PerformersService(performers_repo)
performers_blueprint = PerformersBlueprint(performers_service)

comments_repo = CommentsRepository(db)
comments_service = CommentsService(comments_repo)
comments_blueprint = CommentsBlueprint(comments_service)

notifications_repo = NotificationsRepository(db)
notifications_service = NotificationsService(notifications_repo)
notifications_blueprint = NotificationsBlueprint(notifications_service)

auth_blueprint = AuthBlueprint(users_service)

app.register_blueprint(users_blueprint.blueprint, url_prefix='/users')
app.register_blueprint(tasks_blueprint.blueprint, url_prefix='/tasks')
app.register_blueprint(performers_blueprint.blueprint, url_prefix='/performers')
app.register_blueprint(comments_blueprint.blueprint, url_prefix='/comments')
app.register_blueprint(notifications_blueprint.blueprint, url_prefix='/notifications')
app.register_blueprint(auth_blueprint.blueprint, url_prefix='/')


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
